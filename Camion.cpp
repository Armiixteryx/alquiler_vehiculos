#include "Camion.h"

Camion::Camion()
{
}

float Camion::calcularPrecio(const int dias, const float cargaPermitida)
{
	return TASA_FIJA + Carga::calcularPrecio(dias, cargaPermitida);
}

void Camion::setMatricula(char* newMatricula)
{
	Vehiculo::setMatricula(newMatricula);
}

char* Camion::getMatricula()
{
	return Vehiculo::getMatricula();
}
