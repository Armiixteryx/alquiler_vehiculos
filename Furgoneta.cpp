#include "Furgoneta.h"

Furgoneta::Furgoneta()
{
}

float Furgoneta::calcularPrecio(const int dias, const float cargaPermitida)
{
	return Carga::calcularPrecio(dias, cargaPermitida);
}

void Furgoneta::setMatricula(char* newMatricula)
{
	Vehiculo::setMatricula(newMatricula);
}

char* Furgoneta::getMatricula()
{
	return Vehiculo::getMatricula();
}
