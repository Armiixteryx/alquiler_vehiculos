#ifndef TRANSPORTE_H
#define TRANSPORTE_H

#include "Vehiculo.h"

class Transporte : public Vehiculo
{
	public:
		Transporte();
		Transporte(char* matricula);
		virtual float calcularPrecio(const int dias);
		
		const float TASA_DIA = 1.5;
	protected:
};

#endif
