#ifndef VEHICULO_H
#define VEHICULO_H

class Vehiculo
{
	public:
		Vehiculo();
		Vehiculo(char* matricula);
		
		virtual float calcularPrecio(const int dias);
		
		virtual void setMatricula(char* newMatricula);
		virtual char* getMatricula();
		
		const float BASE_DIA = 50;
	protected:
		char matricula[10];
	private:
		int compareTo(Vehiculo *);
		void leerDatos();
};

#endif
