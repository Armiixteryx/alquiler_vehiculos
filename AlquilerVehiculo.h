#ifndef ALQUILERVEHICULO_H
#define ALQUILERVEHICULO_H

#include <iostream>
#include <cstdlib>
#include <vector>
#include "Vehiculo.h"

class AlquilerVehiculo
{
	public:
		AlquilerVehiculo();
		~AlquilerVehiculo();
		
		int menuPrincipal();
	protected:
		
	private:
		int menuAgregarVehiculo();
		int menuPrecioAlquiler();
		void opcionIncorrecta();
		
		std::vector <Vehiculo> memPrograma;
		
};

#endif
