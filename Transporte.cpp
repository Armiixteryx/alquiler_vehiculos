#include "Transporte.h"

Transporte::Transporte()
{
}

Transporte::Transporte(char* matricula) : Vehiculo(matricula)
{
}

float Transporte::calcularPrecio(const int dias)
{
	if(dias !=0) {
		return (TASA_DIA * (float)dias) + Vehiculo::calcularPrecio(dias);
	} else {
		return 0;
	}
}
