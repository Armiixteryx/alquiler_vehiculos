#include "Coche.h"

Coche::Coche()
{
}

Coche::Coche(char* matricula) : Transporte(matricula)
{
}

float Coche::calcularPrecio(const int dias)
{
	return Transporte::calcularPrecio(dias);
}

void Coche::setMatricula(char* newMatricula)
{
	Vehiculo::setMatricula(newMatricula);
}

char* Coche::getMatricula()
{
	return Vehiculo::getMatricula();
}
