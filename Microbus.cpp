#include "Microbus.h"

Microbus::Microbus()
{
}

Microbus::Microbus(char* matricula) : Transporte(matricula)
{
}

float Microbus::calcularPrecio(const int dias)
{
	return TASA_UNICA + Transporte::calcularPrecio(dias);
}

void Microbus::setMatricula(char* newMatricula)
{
	Vehiculo::setMatricula(newMatricula);
}

char* Microbus::getMatricula()
{
	return Vehiculo::getMatricula();
}
