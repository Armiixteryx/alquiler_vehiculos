#ifndef COCHE_H
#define COCHE_H

#include "Transporte.h"

class Coche : public Transporte
{
	public:
		Coche();
		Coche(char* matricula);
		float calcularPrecio(const int dias);
		void setMatricula(char* newMatricula);
		char* getMatricula();
	protected:
};

#endif
