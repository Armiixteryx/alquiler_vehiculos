#ifndef CAMION_H
#define CAMION_H

#include "Carga.h"

class Camion : public Carga
{
	public:
		Camion();
		Camion(char* matricula);
		float calcularPrecio(const int dias, const float cargaPermitida);
		void setMatricula(char* newMatricula);
		char* getMatricula();
		
		const float TASA_FIJA = 40;
	protected:
};

#endif
