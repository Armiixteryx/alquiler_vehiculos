#ifndef MICROBUS_H
#define MICROBUS_H

#include "Transporte.h"

class Microbus : public Transporte
{
	public:
		Microbus();
		Microbus(char* matricula);
		float calcularPrecio(const int dias);
		void setMatricula(char* newMatricula);
		char* getMatricula();
		
		const float TASA_UNICA = 2;
	protected:
};

#endif
