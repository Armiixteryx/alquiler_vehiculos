#include "Carga.h"

Carga::Carga()
{
}

float Carga::calcularPrecio(const int dias, const float cargaPermitida)
{
	return (TASA_PESO * cargaPermitida) + Vehiculo::calcularPrecio(dias);
}
