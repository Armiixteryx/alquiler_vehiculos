#ifndef CARGA_H
#define CARGA_H

#include "Vehiculo.h"

class Carga : public Vehiculo
{
	public:
		Carga();
		virtual float calcularPrecio(const int dias, const float cargaPermitida);
		
		const float TASA_PESO = 20;
	protected:
};

#endif
