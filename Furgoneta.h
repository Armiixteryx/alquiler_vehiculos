#ifndef FURGONETA_H
#define FURGONETA_H

#include "Carga.h"

class Furgoneta : public Carga
{
	public:
		Furgoneta();
		Furgoneta(char* matricula);
		float calcularPrecio(const int dias, const float cargaPermitida);
		void setMatricula(char* newMatricula);
		char* getMatricula();
		
	protected:
};

#endif
