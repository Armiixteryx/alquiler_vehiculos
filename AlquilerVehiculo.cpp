#include <iostream>
#include <vector>
#include "AlquilerVehiculo.h"
#include "Vehiculo.h"

using namespace std;

AlquilerVehiculo::AlquilerVehiculo()
{
}

AlquilerVehiculo::~AlquilerVehiculo()
{
}

int AlquilerVehiculo::menuPrincipal()
{
	short int opcion;
	
	do {
		system("cls");
		cout << "|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||" << endl;
		cout << "- - - -   A L Q U I L E R   D E   V E H I C U L O S   - - - -" << endl;
		cout << "|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||" << endl << endl;
		
		cout << "    Seleccione su opcion:" << endl;
		cout << "------------------------------" << endl;
		cout << "1. Agregar vehiculo." << endl;
		cout << "2. Obtener precio de alquiler." << endl;
		cout << "3. Salir." << endl;
		cout << "------------------------------" << endl;
		cout << "Su opcion: ";
		cin >> opcion;
		
		switch(opcion) {
			case 1: menuAgregarVehiculo(); break;
			case 2: menuPrecioAlquiler(); break;
			default: break;
		}
		
	} while(opcion != 3);
	
	return 1;
}

int AlquilerVehiculo::menuAgregarVehiculo()
{
	short int tipoVehiculo;
	short int selVehiculo;
	char matricula[10];
	
	do {
		do {
			system("cls");
			
			cout << "|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||" << endl;
			cout << "- - - -   A L Q U I L E R   D E   V E H I C U L O S   - - - -" << endl;
			cout << "|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||" << endl << endl;
			
			cout << "A G R E G A R   V E H I C U L O" << endl;
			cout << "-------------------------------------------------------------" << endl;
			cout << "Elige el tipo de vehiculo:" << endl;
			cout << "1. De transporte humano." << endl;
			cout << "2. De carga." << endl;
			cout << "3. Regresar." << endl;
			cout << "Su opcion: ";
			
			cin.sync();
			cin >> tipoVehiculo;
			if(tipoVehiculo < 1 || tipoVehiculo > 3) {
				opcionIncorrecta();
			}
		} while(tipoVehiculo < 1 || tipoVehiculo > 3);
		
		if(tipoVehiculo != 3) {
			do {
				system("cls");
				
				cout << "|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||" << endl;
				cout << "- - - -   A L Q U I L E R   D E   V E H I C U L O S   - - - -" << endl;
				cout << "|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||" << endl << endl;
				
				cout << "A G R E G A R   V E H I C U L O" << endl;
				cout << "-------------------------------------------------------------" << endl;
				if(tipoVehiculo == 1) {
					cout << "1. Camion." << endl;
					cout << "2. Furgoneta." << endl;
				} else if(tipoVehiculo == 2) {
					cout << "1. Microbus." << endl;
					cout << "2. Coche." << endl;
				}
				cin.sync();
				cin >> selVehiculo;
			} while(selVehiculo < 1 || selVehiculo > 2);
			
			
			
			cout << "Introduce la matricula (max 10): " << endl;
			cin.sync();
			cin.getline(matricula, 10);
			
		}
		
	} while(tipoVehiculo != 3);
}

int AlquilerVehiculo::menuPrecioAlquiler()
{
}

void AlquilerVehiculo::opcionIncorrecta()
{
	cout << endl << "Opcion incorrecta. Intente otra vez.";
	cin.get();
	cin.sync();
}
