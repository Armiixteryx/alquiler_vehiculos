#include "Vehiculo.h"

#include <cstring>

using namespace std;

Vehiculo::Vehiculo()
{
	strcpy(matricula, "");
}

Vehiculo::Vehiculo(char* matricula)
{
	strcpy(this->matricula, matricula);
}

float Vehiculo::calcularPrecio(const int dias)
{
	if(dias != 0) {
		return BASE_DIA * (float)dias;
	}
	else {
		return 0;
	}
}

void Vehiculo::setMatricula(char* newMatricula)
{
	strcpy(matricula, newMatricula);
}

char* Vehiculo::getMatricula()
{
	return matricula;
}

int Vehiculo::compareTo(Vehiculo *)
{
}

void Vehiculo::leerDatos()
{
}
